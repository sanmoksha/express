//debug
const debug = require('debug')('app:startup');
//config
const config = require('config');
//middleware
const logger = require('./middleware/logger');
const helmet = require('helmet');
var morgan = require('morgan');
//express
const express = require('express');
const app = express();
//mongodb
const mongoose = require('mongoose');
// mongoose.connect('mongodb://localhost');
//port
const port = process.env.PORT || 8000;
//routes
const Genre = require('./routes/genres');
const customers = require('./routes/customers');
const Home = require('./routes/home');




//TEMPLATING ENGINE using pug.
app.set('view engine', 'pug');
app.set('views', './views');

//midlleware 
app.use(express.json());
app.use(logger);
app.use(express.static('public'));
app.use(helmet());  

//Configuration
// console.log('Application Name:' + config.get('name'));
// console.log('Mail server:' + config.get('mail.host'));
// console.log('Mail password:' + config.get('mail.password'));

//middleware
if(app.get('env') === 'development') {
  app.use(morgan('tiny')); 
  console.log('morgan enabled...'); 
  
  // debug('morgan enabled...'); //debug
}

//routes
app.use('/api/genres', Genre);
app.use('/api/customers', customers);
app.use('/', Home)

mongoose.connect('mongodb://localhost/vidly')
  .then(() => console.log('Connected to MongoDB...'))
  .catch(err => console.error('Could not connect to MongoDB...'));

app.listen(port);
console.log("server started on port " + port)




